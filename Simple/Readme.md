* `simple             0.035 => 0.002`
* `simplecall         0.005 => 0.001`
* `simpleucall        0.018 => 0.001`
* `simpleudcall       0.022 => 0.001`
* `mandel             0.206 => 0.007`
* `mandel2            0.198 => 0.008`
* `ackermann(7)       0.038 => 0.010`
* `ary(50000)         0.005 => 0.007`
* `ary2(50000)        0.005 => 0.005`
* `ary3(2000)         0.039 => 0.013`
* `fibo(30)           0.069 => 0.035`
* `hash1(50000)       0.006 => 0.010`
* `hash2(500)         0.010 => 0.007`
* `heapsort(20000)    0.036 => 0.013`
* `matrix(20)         0.040 => 0.011`
* `nestedloop(12)     0.058 => 0.010`
* `sieve(30)          0.013 => 0.004`
* `strcat(200000)     0.006 => 0.005`
------------------------
* Total              0.810 => 0.149
------------------------