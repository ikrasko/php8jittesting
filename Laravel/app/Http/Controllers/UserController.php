<?php

namespace App\Http\Controllers;


class UserController extends Controller
{
    public function okString()
    {
        return 'ok';
    }

    public function testStatic()
    {
        $time_end = microtime(true);
        return response()->view('welcome', [
            'total_time' => round($time_end - $GLOBALS['time_start'], 4) . 's',
        ]);
    }

    public function testLoops()
    {
        $a = 0;
        for ($i = 0; $i < 10000000; $i++)
            $a++;

        $anotherLongName = 0;
        for ($longName = 0; $longName < 10000000; $longName++)
            $anotherLongName++;

        $time_end = microtime(true);
        return response()->json([
            'total_time' => round($time_end - $GLOBALS['time_start'], 4) . 's',
        ]);
    }

    public function testCollections()
    {
        $users = \App\Models\User::limit(3000)->get();
        $users = $users->map(function ($user) {
            $user->name = $user->name . 'str';
            return $user;
        });
        $users = $users->map(function ($user) {
            $user->name = $user->name . 'str1';
            return $user;
        });
        $users = $users->map(function ($user) {
            $user->name = $user->name . 'str2';
            return $user;
        });
        $users = $users->map(function ($user) {
            $user->name = $user->name . 'str';
            return $user;
        });
        $users = $users->map(function ($user) {
            $user->name = $user->name . 'str1';
            return $user;
        });
        $users = $users->map(function ($user) {
            $user->name = $user->name . 'str2';
            return $user;
        });
        $users = $users->sortBy('name');
        $users = $users->reverse();
        $users = $users->pluck('id');

        $time_end = microtime(true);
        return response()->json([
            'data' => round($time_end - $GLOBALS['time_start'], 4) . 's'
        ]);
    }

    public function testDynamic()
    {
        \App\Models\User::where('name', 'like', '%Mr%')
            ->limit(10)
            ->update(['name' => \DB::raw('CONCAT(name, id)')]);
        $users = \App\Models\User::where('name', 'like', '%Mr%')->limit(10)->get();

        $time_end = microtime(true);
        return response()->json([
            'users' => $users,
            'time' => round($time_end - $GLOBALS['time_start'], 4) . 's'
        ]);
    }
}
