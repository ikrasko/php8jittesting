### Build
docker-compose up -d --build

### Reload php configs
docker-compose restart php8-fpm-laravel

### run test

* `ab -k -n 500 -c 30 -l http://localhost:8085/test-ok/`
* `ab -k -n 500 -c 30 -l http://localhost:8085/test-static/`

####  
* "-k" Enable the HTTP KeepAlive feature, i.e., 
    perform multiple requests within one HTTP session. 
    Default is no KeepAlive.
* "-n" Number of requests to perform for the benchmarking session.
    The default is to just perform a single request which usually leads
    to non-representative benchmarking results.
* "-c" Number of multiple requests to perform at a time. Default is one request at a time.
* "-l" Do not report errors








==========================
https://wiki.php.net/rfc/jit

`opcache.jit=1205 (JIT everything)`

`opcache.jit=1235 (JIT hot code based on relative usage)`

`opcache.jit=1255 (trace hot code for JITability, the best so far)`

C - CPU specific optimization flags
   0 - none
   1 - enable AVX instruction generation  - Advanced Vector Extensions
           расширение системы команд улучшенными, не все процессоры их поддерживают
R - register allocation
   0 - don't perform register allocation
   1 - use local liner-scan register allocator
   2 - use global liner-scan register allocator
           как я понял зависит от архитектуры и с register allocator более оптимально
T - JIT trigger
   0 - JIT all functions on first script load
           при первой загрузке обработает все
   1 - JIT function on first execution
           только то что выполняется
   2 - Profile on first request and compile hot functions on second request
           прочитает на первом запросе и на втором закомпилирует частоиспользуемые части
   3 - Profile on the fly and compile hot functions
           только самые юзабельные части кода
   4 - Compile functions with @jit tag in doc-comments
O - Optimization level
   0 - don't JIT
   1 - minimal JIT (call standard VM handlers)
   2 - selective VM handler inlining
   3 - optimized JIT based on static type inference of individual function
   4 - optimized JIT based on static type inference and call tree
   5 - optimized JIT based on static type inference and inner procedure analyses


Areas that may benefit from JIT include:

 Serialization/Deserialization
 Routing
 Hashing functions
 Image processing
