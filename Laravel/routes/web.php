<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/info', function () {
    echo phpinfo();
});

Route::get('/test-ok', [UserController::class, 'okString']);
Route::get('/test-static', [UserController::class, 'testStatic']);
Route::get('/test-loops', [UserController::class, 'testLoops']);
Route::get('/test-collections', [UserController::class, 'testCollections']);
Route::get('/test-dynamic', [UserController::class, 'testDynamic']);

//Route::get('/test-collections', [UserController::class, 'test']);
//Route::get('/test-collections', [UserController::class, 'test']);
